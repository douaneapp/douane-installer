import gettext
import os

current_path = os.path.abspath(os.path.dirname(__file__))
localedir = os.path.join(current_path, '..', '..', 'locale')
translate = gettext.translation('base', localedir, fallback=True,
                                languages=[os.environ['LANG']])
_ = translate.gettext
