from gi.repository import GdkPixbuf
from gi.repository import Gtk


class ImageBuilder:
    def __init__(self, path, size):
        self.path = path
        self.size = size
        self.preserve_aspect_ratio = True

    def build(self):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
            self.path,
            -1,
            self.size,
            self.preserve_aspect_ratio
        )

        image = Gtk.Image()
        image.set_from_pixbuf(pixbuf)
        return image
