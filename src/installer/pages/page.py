from gi.repository import GObject
from gi.repository import Gtk


class Page(Gtk.Box):
    def __init__(self, assistant):
        GObject.GObject.__init__(self, orientation=Gtk.Orientation.VERTICAL)

        self.assistant = assistant
        self.page_title = None
        self.page_type = None

    def build_page(self):
        return self.page_content(self)
