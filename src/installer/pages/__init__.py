from .install_page import InstallPage
from .summary_page import SummaryPage
from .unsupported_os_page import UnsupportedOsPage
from .unsupported_os_distribution_page import UnsupportedOsDistributionPage
from .uninstall_page import UninstallPage
from .welcome_page import WelcomePage
