import logging

from installer.pages import InstallPage, SummaryPage, UninstallPage, WelcomePage
from installer.pages import UnsupportedOsPage, UnsupportedOsDistributionPage


class AssistantPagesBuilder:
    """
    The AssistantPagesBuilder class adds pages to the Assistant based on
    the given configuration.
    """

    def __init__(self, assistant, configuration):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.assistant = assistant
        self.configuration = configuration

    def add_installer_pages(self):
        # Welcome
        self.assistant.add_page(WelcomePage(self.assistant).build_page())

        # Install
        self.assistant.install_page = InstallPage(self.assistant,
                                                  self.configuration)
        self.assistant.add_page(self.assistant.install_page.build_page(),
                                False)

        # Summary
        self.assistant.summary_page = SummaryPage(self.assistant)
        self.assistant.add_page(self.assistant.summary_page.build_page())

    def add_uninstaller_pages(self):
        # Welcome
        self.assistant.add_page(
            WelcomePage(self.assistant, uninstall=True).build_page()
        )

        # Uninstall
        self.assistant.install_page = UninstallPage(self.assistant,
                                                    self.configuration)
        self.assistant.add_page(self.assistant.install_page.build_page(),
                                False)

        # Summary
        self.assistant.summary_page = SummaryPage(self.assistant)
        self.assistant.add_page(self.assistant.summary_page.build_page())

    def add_unsupported_os_distribution(self):
        self.assistant.add_page(
            UnsupportedOsDistributionPage(
                self.assistant, self.configuration
            ).build_page()
        )

    def add_unsupported_os_page(self):
        self.assistant.add_page(UnsupportedOsPage(self.assistant).build_page())

    def build_pages(self):
        self.logger.debug(f'self.configuration.os: {self.configuration.os}')
        if self.supported_os():
            if self.supported_linux_distribution():
                if self.configuration.uninstall:
                    self.add_uninstaller_pages()
                else:
                    self.add_installer_pages()
            else:
                self.add_unsupported_os_distribution()
        else:
            self.add_unsupported_os_page()

    def supported_linux_distribution(self):
        return self.configuration.debian_based

    def supported_os(self):
        return self.configuration.os == 'Linux'
