import logging


from douane.command_executor import CommandExecutor

class AptPackageManager:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def install(self, package_list):
        self.logger.debug(f'installing {package_list} ...')

        command = self.__build_install_command(package_list)
        completed_process = CommandExecutor().execute(command)

        if completed_process.returncode != 0:
            self.__on_failure_callback(completed_process.stderr.decode("utf-8"))

        return completed_process.returncode == 0

    def uninstall(self, package_list):
        self.logger.debug(f'uninstalling {package_list} ...')

        command = self.__build_uninstall_command(package_list)
        completed_process = CommandExecutor().execute(command)

        if completed_process.returncode != 0:
            self.__on_failure_callback(completed_process.stderr.decode("utf-8"))

        return completed_process.returncode == 0

    def on_failure(self, callback):
        self.__on_failure_callback = callback

    def __build_install_command(self, package_list):
        return ["apt", "install", "--yes"] + package_list

    def __build_uninstall_command(self, package_list):
        return ["apt", "remove", "--purge", "--yes"] + package_list
