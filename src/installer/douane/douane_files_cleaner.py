import logging
import os
import shutil


from douane.compiler import DouaneCompiler

class DouaneFilesCleaner():
    def __init__(self, configuration, on_failure):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration
        self.on_failure = on_failure

    def clean(self):
        self.logger.debug('Killing douane if running ...')

        self.__update_configuration_sources_path()

        try:
            self.__uninstall_dialog_daemon_and_dkms()
            self.__clean_up_douane_folders()
            self.__clean_up_douane_archives()
        except FileNotFoundError:
            pass

    def __clean_up_douane_folders(self):
        """
        rm -rf the source folders from `/usr/src`
        """
        for repository in self.configuration.repositories:
            module_folder_path = repository["sources_path"]
            self.logger.debug(f'Removing the folder {module_folder_path} ...')
            shutil.rmtree(module_folder_path, ignore_errors=True)

    def __clean_up_douane_archives(self):
        """
        rm -rf the source folders from `/usr/src`
        """
        for repository in self.configuration.repositories:
            archive_path = ".".join([
                repository["sources_path"],
                self.configuration.douane_archive_extension
            ])

            self.logger.debug(f'Removing the archive {archive_path} ...')
            os.remove(archive_path)

    def __uninstall_dialog_daemon_and_dkms(self):
        douane_compiler = DouaneCompiler(configuration=self.configuration,
                                         on_failure=self.on_failure)
        return douane_compiler.uninstall()

    def __update_configuration_sources_path(self):
        for index, repository in enumerate(
            self.configuration.repositories,
            start=1
        ):
            path = os.path.join(self.configuration.working_directory,
                                repository["source_folder_name"])

            self.configuration.repositories[(index - 1)]["sources_path"] = path
