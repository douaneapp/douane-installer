import logging


try:
    from installer.douane.command_executor import CommandExecutor
except ModuleNotFoundError:
    from douane.command_executor import CommandExecutor

class Systemctl():
    def __init__(self, on_failure, user = False):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.on_failure = on_failure
        self.user = user

    def reload(self):
        self.logger.debug('reloading systemctl daemon ...')

        command = self.__build_reload_command()
        return self.__run_command_and_check_exit_code(command)

    def enable(self, module):
        self.logger.debug(f'enabling module {module} ...')

        command = self.__build_enable_command(module)
        return self.__run_command_and_check_exit_code(command)

    def start(self, module):
        self.logger.debug(f'starting module {module} ...')

        command = self.__build_start_command(module)
        return self.__run_command_and_check_exit_code(command)

    def stop(self, module):
        self.logger.debug(f'stopping module {module} ...')

        command = self.__build_stop_command(module)
        return self.__run_command_and_check_exit_code(command)

    def __build_systemctl_command(self, action, module = None):
        if self.user:
            if module:
                return ["systemctl", "--user", action, module]
            else:
                return ["systemctl", "--user", action]
        else:
            return ["systemctl", action, module]

    def __build_enable_command(self, module):
        return self.__build_systemctl_command("enable", module)

    def __build_reload_command(self):
        return self.__build_systemctl_command("daemon-reload")

    def __build_start_command(self, module):
        return self.__build_systemctl_command("start", module)

    def __build_stop_command(self, module):
        return self.__build_systemctl_command("stop", module)

    def __run_command_and_check_exit_code(self, command):
        self.logger.debug(f'running command {command} ...')
        process = CommandExecutor().execute(command)

        self.logger.debug(f'command {command} exit code is '
                          f'{process.returncode}')

        if process.returncode != 0:
            self.on_failure(process.stderr.decode("utf-8"))

        return process.returncode == 0
