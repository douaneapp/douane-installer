import logging


from douane.dependencies import DebianDependencies


class DependenciesManagerFactory:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

        self._creators = {}

        self.__register_all_formats()

    def register_distribution(self, name, creator):
        self.logger.debug(f'Registering distribution {name}')
        self._creators[name] = creator

    def create(self, name, configuration):
        creator = self._creators.get(name)

        if not creator:
            raise ValueError(name)

        self.logger.debug(f'Returning creator for name {name}')
        return creator(configuration)

    def __register_all_formats(self):
        self.register_distribution('debian', DebianDependencies)
        self.register_distribution('Ubuntu', DebianDependencies)
        self.register_distribution('elementary', DebianDependencies)
