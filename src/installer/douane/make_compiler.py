import logging


from douane.command_executor import CommandExecutor

class MakeCompiler():
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def name(self):
        return 'Make'

    def compile(self, sources_path):
        self.logger.debug(f'Compiling Make module from {sources_path} ...')

        command = self.__build_compile_command()
        completed_process = CommandExecutor().execute(command, sources_path)

        return completed_process.returncode == 0

    def uninstall(self, sources_path):
        self.logger.debug(f'Uninstalling Make module from {sources_path} ...')

        try:
            command = self.__build_uninstall_command()
            completed_process = CommandExecutor().execute(command, sources_path)

            return completed_process.returncode == 0
        except FileNotFoundError:
            pass

    def __build_compile_command(self):
        return ["make", "install"]

    def __build_uninstall_command(self):
        return ["make", "uninstall"]
