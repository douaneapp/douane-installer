import logging


from douane.decompression_manager_factory import DecompressionManagerFactory
from douane.dkms_compiler import DkmsCompiler
from douane.download_manager_factory import DownloadManagerFactory
from douane.make_compiler import MakeCompiler
from douane.python_installer import PythonInstaller
from douane.symlinker import Symlinker

class DouaneCompiler():
    def __init__(self, on_failure, configuration):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.on_failure = on_failure
        self.configuration = configuration

    def compile(self):
        self.logger.debug('compile ...')

        for repository in self.configuration.repositories:
            self.logger.debug(f'Compiling {repository["name"]} from '
                              f'{repository["sources_path"]} ...')

            compiler = self.__initialize_compiler_for(repository)

            if compiler:
                self.logger.debug(f'Running the {compiler.name()} compiler ...')

                if compiler.compile(repository["sources_path"]):
                    self.logger.debug(
                        f'{compiler.name()} compiler successfully '
                        f'compiled {repository["name"]}'
                    )
                else:
                    self.logger.debug(f'{compiler.name()} compiler failed to '
                                      f'compile {repository["name"]}')
                    return False # Breaks the loop
            else:
                self.logger.debug('No compilation required, skipping ...')

        return True

    def uninstall(self):
        for repository in self.configuration.repositories:
            self.logger.debug(f'Uninstalling {repository["name"]} from '
                              f'{repository["sources_path"]} ...')

            compiler = self.__initialize_compiler_for(repository)

            if compiler:
                self.logger.debug(f'Running the {compiler.name()} compiler ...')

                if compiler.uninstall(repository["sources_path"]):
                    self.logger.debug(
                        f'{compiler.name()} compiler successfully '
                        f'uninstalled {repository["name"]}'
                    )
                else:
                    self.logger.debug(f'{compiler.name()} compiler failed to '
                                      f'uninstall {repository["name"]}')
                    # Let's continue to uninstall the next repository
            else:
                self.logger.debug('No compilation required, skipping ...')

        return True

    def __initialize_compiler_for(self, repository):
        if repository["compilation_type"] == 'dkms':
            return DkmsCompiler()
        elif repository["compilation_type"] == 'make':
            return MakeCompiler()
        elif repository["compilation_type"] == 'python':
            return PythonInstaller()
        elif repository["compilation_type"] == 'symlink':
            return Symlinker(
                options=repository["compilation_options"],
                working_directory=self.configuration.working_directory
            )
        else:
            return None
