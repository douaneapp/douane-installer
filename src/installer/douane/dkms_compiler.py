import logging


from douane.command_executor import CommandExecutor

class DkmsCompiler():
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.sources_path = None

    def name(self):
        return 'DKMS'

    def compile(self, sources_path):
        self.logger.debug(f'Compiling DKMS module from {sources_path} ...')

        self.sources_path = sources_path

        self.__remove_previous_dkms_module()

        return self.__compile_dkms_module()

    def uninstall(self, sources_path):
        self.logger.debug(f'Uninstalling DKMS module from {sources_path} ...')

        self.sources_path = sources_path

        try:
            return self.__remove_previous_dkms_module()
        except FileNotFoundError:
            pass

    def __compile_dkms_module(self):
        command = ["make", "dkms"]
        completed_process = CommandExecutor().execute(command,
                                                      self.sources_path)

        return completed_process.returncode == 0

    def __remove_previous_dkms_module(self):
        command = ["make", "cleandkms"]
        completed_process = CommandExecutor().execute(command,
                                                      self.sources_path)

        if completed_process.returncode == 0:
            self.logger.debug('DKMS clean succeeded')
