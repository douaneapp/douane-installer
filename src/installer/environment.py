import distro
import logging
import os.path
import platform
import shutil


class Environment:
    """
    The Environment class discovers the environment where is running
    the installer and updates a Configuration class instance.
    """
    def __init__(self, configuration):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration

    def discover(self):
        self.__discover_os()

        if self.configuration.os == 'Linux':
            self.__discover_linux_distribution()
            self.__discover_linux_decompression_manager()
            self.__discover_linux_download_manager()
            self.__discover_linux_package_format()

    def is_a_debian_based_distribution(self):
        return os.path.isfile('/etc/debian_version')

    def __discover_os(self):
        self.configuration.os = platform.system()

    def __discover_linux_distribution(self):
        self.configuration.distribution = distro.linux_distribution(
            full_distribution_name=False
        )
        self.configuration.debian_based = self.is_a_debian_based_distribution()

    def __discover_linux_decompression_manager(self):
        if shutil.which('tar') is not None:
            self.configuration.decompression_manager = 'tar'
            self.configuration.douane_archive_extension = 'tar.gz'
        # elif shutil.which('unzip') is not None:
        #     self.configuration.decompression_manager = 'unzip'
        #     self.configuration.douane_archive_extension = 'zip'
        else:
            self.configuration.decompression_manager = None
            self.configuration.douane_archive_extension = None

    def __discover_linux_download_manager(self):
        if shutil.which('wget') is not None:
            self.configuration.download_manager = 'wget'
        # elif shutil.which('curl') is not None:
        #     self.configuration.download_manager = 'curl'
        else:
            self.configuration.download_manager = None

    def __discover_linux_package_format(self):
        if self.configuration.debian_based:
            self.configuration.package_format = 'APT'
