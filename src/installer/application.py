import logging

from gi.repository import Gtk

from installer.assistant_pages_builder import AssistantPagesBuilder
from installer.configuration import Configuration
from installer.environment import Environment

from installer.i18n import _

ASSISTANT_WINDOW_WIDTH = 800
ASSISTANT_WINDOW_HEIGHT = 600


class Application(Gtk.Assistant):
    def __init__(self, args):
        Gtk.Assistant.__init__(self)
        self.__init_gtk_assistant()

        self.logger = logging.getLogger(self.__class__.__name__)

        self.pages = {}
        self.configuration = Configuration(args=args)

        self.__connect_events()
        self.__detect_os_and_linux_distribution()
        self.__build_assistant_pages()

    def __build_assistant_pages(self):
        """
        Builds the Assistant pages.

         - In the case of an unsupported OS, it adds the UnsupportedOsPage
         page to the assistant
         - In the case of an unsopported Linux distribution it adds the
         UnsupportedLinuxDistroPage to the assistant
         - Otherwise it adds the WelcomePage, InstallPage and SummaryPage pages
         to the assistant
        """

        AssistantPagesBuilder(self, self.configuration).build_pages()

    def __connect_events(self):
        """
        Connects Gtk.Assistant events so that we can quit Gtk look in the case
        the Cancel or Close buttons are clicked, and also detect Next button
        click, thanks to the `prepare` signal, so that we can trigger a method
        on the next page (This is used for the InstallPage, in order to start
        installing Douane).
        """
        self.connect('cancel', self.on_close_or_cancel_clicked)
        self.connect('close', self.on_close_or_cancel_clicked)
        self.connect('prepare', self.on_prepare_clicked)

    def __detect_os_and_linux_distribution(self):
        """
        Detects the OS and in the case of Linux OS, detects the distribution.

        Then it populates a configuration object used later in order to show an
        unsupported OS or Linux distribution page, or in order to run the right
        command for the supported Linux distros.
        """
        Environment(self.configuration).discover()

    def __init_gtk_assistant(self):
        self.set_title(_('Douane Installer'))
        self.set_default_size(ASSISTANT_WINDOW_WIDTH, ASSISTANT_WINDOW_HEIGHT)
        self.set_position(Gtk.WindowPosition.CENTER)

    def add_page(self, page, complete_page=True):
        """
        Adds the given page instance to the Gtk.Assistant and stores that page
        in the `pages` dictionnary.
        """
        self.append_page(page)
        self.set_page_type(page, page.page_type)
        self.set_page_title(page, page.page_title)

        self.logger.debug(f'Setting the page {page.__class__.__name__} '
                          f'complete as {complete_page}')
        self.set_page_complete(page, complete_page)

        # Saving pages instances in a dictionnary have the page position as key
        self.pages[len(self.pages)] = page

    def on_close_or_cancel_clicked(self, *args):
        """
        Triggerd on clicking the "Close" or "Cancel" buttons from the Assistant
        """
        Gtk.main_quit()

    def on_prepare_clicked(self, *args):
        """
        Triggered on clicking the Next button. This method then triggers a
        specific method on the next page to be shown.

        This is used by the InstallPage which then starts to install Douane.
        """
        current_page = self.get_current_page()

        # Retrieves the page object from its index from the pages dictionnary
        page = self.pages[current_page]

        # Fires the `on_page_prepared` method in the case the page object
        # implements it.
        if "on_page_prepared" in dir(page):
            self.logger.debug('on_prepare_clicked: Calling on_page_prepared '
                              'method ...')

            page.on_page_prepared()
