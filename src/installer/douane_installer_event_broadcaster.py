import logging
import os


class DouaneInstallerEventBroadcaster:
    """
    Runs the DouaneInstaller as root via pkexec so that the root password is
    asked only once.
    """

    def __init__(self, configuration):
        self.logger = logging.getLogger(self.__class__.__name__)

        self.configuration = configuration
        self.fifo = None

    def connect(self):
        """
        Open the pipe where to push events.
        """
        self.logger.debug(
            f'Opening the pipe from {self.configuration.douane_pipe_path} ...'
        )

        self.fifo = os.open(self.configuration.douane_pipe_path, os.O_WRONLY)

        return True

    def broadcast(self, event, args={}):
        try:
            os.write(self.fifo, f'{event}|{args}\n'.encode())
        except BrokenPipeError:
            pass
